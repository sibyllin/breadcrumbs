import { LightningElement } from "lwc";
import Menubar from "./menubar";

export default class App extends LightningElement {
  crumbCounter = 4
  eventValue ='';
  
  get breadcrumbElement(){
    return this.template.querySelector(Menubar.SELECTOR); 
  }

  handleAddCrumbClick(){
    const currentBreadcrumbs = this.breadcrumbElement.breadcrumbs;
    this.breadcrumbElement.breadcrumbs =  [...currentBreadcrumbs, {
            textDisplay : 'lbl Child field ' + this.crumbCounter, 
            label: 'lbl Child field ' + this.crumbCounter,
            name: 'apiNameChild' + this.crumbCounter,
            id: 'account___' + this.crumbCounter
        }];
        ++this.crumbCounter;
        
  }

  handleMenuitemClick(event){
    this.eventValue = JSON.stringify(event.detail, null, 2);
  }

  renderedCallback(){
    if(!this.initialized){
      this.initialized = true;
      this.breadcrumbElement.breadcrumbs = [
            { textDisplay: 'vFeedItem', label: 'vFeedItem', name: 'vFeedItem', id: 'vFeedItem_0' },
            { textDisplay: 'CreatedBy (User)', label: 'CreatedBy (User)', name: 'CreatedBy:User', id: 'CreatedBy:User_1' },
            { textDisplay: 'Account', label: 'Account', name: 'Account', id: 'Account_2' },
            { textDisplay: 'lName', label: 'Name', name: 'Name', id: 'Name_3' }
        ];
    } 
  }
}
