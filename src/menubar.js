
import { api, LightningElement, track } from 'lwc';

const ROOT_PARENT_NAME = 'All'
const ROOT_PARENT = { textDisplay:'All', label: 'All', name: ROOT_PARENT_NAME, id: ROOT_PARENT_NAME };

export default class Menubar extends LightningElement { 
    static SELECTOR = 'c-menubar';
    static CLICK_EVENT_NAME = 'menuitemclick';
    
    @api
    breadcrumbs = [];

    @api
    swap(swapApiNameLabelvalue){
      this.breadcrumbs = [...this.breadcrumbs].map(b => Object.assign({}, b, {textDisplay : b[swapApiNameLabelvalue]}));
     }

    @track
    truncatedBreadcrumbs = [ROOT_PARENT];
 
    get breadcrumbsComputed(){
      return this.breadcrumbs.length > 0 ? this.isTruncated ? this.breadcrumbs : [ROOT_PARENT, ...this.breadcrumbs] : [];
    }


    get isTruncated() {
        return this.truncatedBreadcrumbs.length > 1; 
    }

    get isTruncationNeeded() {
        const menuBar = this.template.querySelector('ol');
        return menuBar && menuBar.offsetWidth < menuBar.scrollWidth;
    }

    reset(){
        this.breadcrumbs = [];
    }

    updateBreadcrumbs(index){   
        index === -1 ? this.reset(): this.breadcrumbs = this.breadcrumbs.slice(0, index+1);
    }
    
    getSelectedBreadcrumbPath(index){
        return this.breadcrumbs.map(b => b.name).join('.');
    }

    handleBreadcrumbClick(event) {
        event.preventDefault();
        
        const {target:{name : breadcrumbName}} = event;
        const index = this.breadcrumbs.findIndex((breadcrumb) => breadcrumb.name === breadcrumbName);

        this.updateBreadcrumbs(index);
        
        this.dispatchEvent(new CustomEvent(Menubar.CLICK_EVENT_NAME, {
            composed: true, 
            bubbles: true,
            detail: {
                breadcrumb: {
                ...this.breadcrumbs[index], index , path : this.getSelectedBreadcrumbPath(index)
                }
            }
        }));
    }

    renderedCallback() {

      console.log("this.isTruncationNeeded", this.isTruncationNeeded);
      if (this.isTruncationNeeded) {
           
            console.log("this.breadcrumbs", this.breadcrumbs);
            const [firstBreadcrumb] = this.breadcrumbs;
            this.truncatedBreadcrumbs = [...this.truncatedBreadcrumbs,firstBreadcrumb ];
            this.breadcrumbs = this.breadcrumbs.slice(1)
            console.log("updated this.breadcrumbs", this.breadcrumbs);
            
      }
    }
}
